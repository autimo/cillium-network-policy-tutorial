# Cilium Network Policy

## Policy Description

This network policy uses different values for `apiVersion` and `kind` as a regular Kubernetes network policy. The Cilium Network Policy is similar, but contains additional features.
The entire file can be found at: [network-policy.yaml](network-policy.yaml).

### Endpoint Selector

```yaml
  endpointSelector:
    matchLabels:
      app: nginx
```

This section states that only resources with label `app: nginx` will be affected by this policy.

### Egress

#### DNS service

```yaml
    - toEndpoints:
      - matchLabels:
          "k8s:io.kubernetes.pod.namespace": kube-system
          "k8s:k8s-app": kube-dns
      toPorts:
        - ports:
          - port: "53"
            protocol: ANY
          rules:
            dns:
              - matchPattern: "*"
```

This section allows all DNS traffic on port 53 to the kubernetes DNS service. This is required to match the IP addresses for the allowed domain name in toFQDNS below.

#### Allowed Domain

```yaml
    - toFQDNs:
      - matchName: "autimo.com"
      toPorts:
        - ports:
          - port: '443'
            protocol: TCP
```

This section allows egress to `autimo.com` on port `443` only. Cilium matches the IP address of autimo.com from the rule and the request using the DNS service and DNS Proxy from above.

#### NGINX Endpoints

```yaml
    - toEndpoints:
      - matchLabels:
          app: nginx
```

This section allows egress to other endpoints with the label `app: nginx` only.

### Ingress

```yaml
  ingress:
  - fromEndpoints:
    - matchLabels:
        app: nginx
```

This section allows ingress from other endpoints with the label `app: nginx` only.

### Notes

- The policy applies to all pods in the namespace it was deployed. In this case it is the "default" namespace.
- All other ingress or egress not specified by the policy is not allowed.

## Deployments

The difference between the 3 deployments is that nginx-deployment-1 and nginx-deployment-2 have the label `app: nginx`, and nginx-deployment-3 has the label `app: nginx-new`.

## Setup

### Kind

These steps are adapted from [Cilium - Getting Started Using Kind](https://docs.cilium.io/en/stable/gettingstarted/kind/).

- [Install these depencies](https://docs.cilium.io/en/stable/gettingstarted/kind/#install-dependencies)
- Create a cluster: `kind create cluster --config=kind-config.yaml`
- Setup Helm repository: `helm repo add cilium https://helm.cilium.io/`
- Install the Cilium release via Helm:

```bash
helm install cilium cilium/cilium --version 1.10.3 \
   --namespace kube-system \
   --set nodeinit.enabled=true \
   --set kubeProxyReplacement=partial \
   --set hostServices.enabled=false \
   --set externalIPs.enabled=true \
   --set nodePort.enabled=true \
   --set hostPort.enabled=true \
   --set bpf.masquerade=false \
   --set image.pullPolicy=IfNotPresent \
   --set ipam.mode=kubernetes
```

- Validate the installation: `kubectl -n kube-system get pods --watch`
- Exit once all components show **1/1** under **READY**

### Pods

- Create all 3 deployments:

  - `kubectl apply -f nginx-deployment-1.yaml`

  - `kubectl apply -f nginx-deployment-2.yaml`

  - `kubectl apply -f nginx-deployment-3.yaml`

- Get the names of the pods: `kubectl get pods`

  - Note down the name of each pod to use later.

- Get a shell into nginx-deployment-1 pod: `kubectl exec -it <nginx-deployment-1 NAME> -- /bin/bash`

- Install curl in nginx-deployment-1:
  - `apt update`
  - `apt install curl`

- Exit the shell

- Get a shell into nginx-deployment-3 pod: `kubectl exec -it <nginx-deployment-3 NAME> -- /bin/bash`

- Install curl in nginx-deployment-3:
  - `apt update`
  - `apt install curl`

- Exit the shell

- Apply the network policy: `kubectl apply -f network-policy.yaml`

## Policy Tracing

Using [Cilium Policy Tracing](https://docs.cilium.io/en/v1.8/cheatsheet/#tracing), we can simulate ingress and egress between two pods.

Command to check policy enforcement:
`cilium policy trace --src-k8s-pod <namespace>:<pod.from> --dst-k8s-pod <namespace>:<pod.to>`

### Testing Ingress/Egress between pods

- Get the cilium agent pod name:

`kubectl get pods -n kube-system`

Note down the name of one of the pods called `cilium-#####` (e.g. "cilium-r688k").

- Get a shell into the cillium agent pod:

`kubectl exec -ti <cillium pod name> -n kube-system -- /bin/bash`

Use the pod names noted down from earlier in the following commands.

- Test that nginx-deployment-1 can talk to nginx-deployment-2:

`cilium policy trace --src-k8s-pod default:<nginx-deployment-1 NAME> --dst-k8s-pod default:<nginx-deployment-2 NAME>`

The output should state: `Final verdict: ALLOWED`

- Test that nginx-deployment-1 cannot talk to nginx-deployment-3:

`cilium policy trace --src-k8s-pod default:<nginx-deployment-1 NAME> --dst-k8s-pod default:<nginx-deployment-3 NAME>`

The output should state: `Final verdict: DENIED`

- Test that nginx-deployment-3 cannot talk to nginx-deployment-1:

`cilium policy trace --src-k8s-pod default:<nginx-deployment-3 NAME> --dst-k8s-pod default:<nginx-deployment-1 NAME>`

The output should state: `Final verdict: DENIED`

- Exit the shell

## Testing Allowed Domain

- Get a shell into nginx-deployment-1 pod:

`kubectl exec -it <nginx-deployment-1 NAME> -- /bin/bash`

- Test egress to autimo.com (Port 443):

`curl -I https://autimo.com`

Should result in success (200 response).

- Test egress to google.com (Port 443):

`curl -I https://google.com`

Should not be successful.

- Test that port 443 is enforced:

`curl -I http://autimo.com`

Should not be successful.

- Exit the shell

## Hubble

### UI Setup

These steps are adapted from [Cilium - Service Map & Hubble UI](https://docs.cilium.io/en/stable/gettingstarted/hubble/)

- Enable the Hubble UI:

```bash
helm upgrade cilium cilium/cilium --version 1.10.3 \
   --namespace kube-system \
   --reuse-values \
   --set hubble.relay.enabled=true \
   --set hubble.ui.enabled=true
```

- Start the Hubble UI: `kubectl port-forward -n kube-system svc/hubble-ui --address 0.0.0.0 --address :: 12000:80`

- Open [http://localhost:12000](http://localhost:12000) in your browser

- Select "default"

### Create Traffic

As you perform the following steps, the service map data should appear in the Hubble UI.

- Repeat the steps in [Testing Allowed Domain](#testing-allowed-domain)

- Get the IP of the pods: `kubectl get pod -o wide`
  - Note down the IPs

- Get a shell into nginx-deployment-1 pod: `kubectl exec -it <nginx-deployment-1 NAME> -- /bin/bash`
- Run: `curl <nginx-deployment-2 IP>`
- Run: `curl <nginx-deployment-3 IP>`

- Get a shell into nginx-deployment-3 pod: `kubectl exec -it <nginx-deployment-3 NAME> -- /bin/bash`
- Run: `curl <nginx-deployment-1 IP>`
- Run: `curl <nginx-deployment-2 IP>`


## Clean Up

To delete all resources, run: `kind delete cluster`.
